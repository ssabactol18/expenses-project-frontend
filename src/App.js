import ExpenseList from './components/ExpenseList'
import ExpenseForm from './components/ExpenseForm'

const App = () => (
  <div className="container">
    <h3>New Expenses</h3>
    <ExpenseForm />
    <hr />
    <h3>Your Expenses</h3>
    <ExpenseList />
  </div>
);

export default App;
