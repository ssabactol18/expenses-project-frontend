import { React, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { GetExpenses } from '../services/expenses'
import { Button, Row, Col } from 'react-bootstrap'
import ExpenseForm from './ExpenseForm'

const ExpenseList = () => {
    const dispatch = useDispatch()
    const expenses = useSelector(state => state.expensesReducer.expenses)

    useEffect(() => {
        GetExpenses(dispatch);
    }, [])

    return expenses.map(expense =>
        <div key={expense.id}>
            <ListRow expense={expense} />
        </div>
    )
}

const ListRow = ({ expense }) => {
    const [isEditing, setIsEditing] = useState(false)

    return isEditing
        ? <ExpenseForm expense={expense} setIsEditing={setIsEditing} key={expense.id} />
        : <div>
            <Row>
                <Col>{expense.description}</Col>
                <Col>$ {expense.amount}</Col>
                <Button variant="warning" onClick={() => setIsEditing(!isEditing)}>Edit</Button>
            </Row>
            <hr />
        </div>


}

export default ExpenseList