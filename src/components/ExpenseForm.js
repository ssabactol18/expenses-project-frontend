import { React, useState, useEffect } from 'react'
import { Form, Row, Col, Button } from 'react-bootstrap'
import { NewExpense, EditExpense, DeleteExpense } from '../services/expenses'
import { useDispatch } from 'react-redux'

const ExpenseForm = ({ expense, setIsEditing, key }) => {
    const descriptions = ['Groceries', 'Electric Bill', 'Water Bill', 'Internet Bill', 'Transportation Fee']
    const [amount, setAmount] = useState(0)
    const [description, setDescription] = useState(descriptions[0])
    const [isNewExpense, setIsNewExpense] = useState(true)
    const dispatch = useDispatch()

    useEffect(() => {
        if (expense !== undefined) {
            setIsNewExpense(false)
            setAmount(expense.amount)
            setDescription(expense.description)
        }
        else {
            setIsNewExpense(true)
        }
    }, [expense])

    return (
        <Form onSubmit={e => {
            e.preventDefault()
            if (isNewExpense) {
                NewExpense(dispatch, { description: description, amount: Number(amount) })
            } else {
                EditExpense(dispatch, { id: expense.id, description: description, amount: Number(amount) })
                setIsEditing(false)
            }
        }}>
            <Row>
                <Col>
                    <Form.Label>Description</Form.Label>
                    <Form.Control as='select' onChange={event => {
                        console.log(event.target.value)
                        setDescription(event.target.value) }}>
                        {descriptions.map((desc, index) =>
                            <ListDesc desc={desc} index={index} />
                        )}
                    </Form.Control>
                </Col>
                <Col>
                    <Form.Label>Amount</Form.Label>
                    <Form.Control
                        type='number'
                        step='0.01'
                        placeholder={amount}
                        onChange={event => setAmount(event.target.value)}>
                    </Form.Control>
                </Col>
                <Col>

                    <Form.Label>Buttons </Form.Label>
                    <br />
                    {isNewExpense ?
                        <Button variant='primary' type='submit' >Add</Button>
                        :
                        <div>

                            <Button variant='danger' style={{ marginRight: '2px' }} onClick={() => {
                                DeleteExpense(dispatch, expense)
                                // setIsEditing(false)
                            }}>Delete</Button>
                            <Button variant='success' type='submit' style={{ marginRight: '2px' }}>Save</Button>
                            <Button variant='warning' onClick={() => setIsEditing(false)}>Cancel</Button>
                        </div>
                    }

                </Col>
            </Row>
        </Form >
    )
}



const ListDesc = ({ desc, index }) => {
    return <option key={index}>{desc}</option>
}



export default ExpenseForm
