import { ActionCreators } from '../app/expensesReducer'
import * as axios from 'axios'

const axiosInstance = axios.create({
    baseURL: 'https://localhost:5001/Expenses'
})

// dispatch is the function of the redux store. Dispatch is used to trigger the state change 
export const GetExpenses = async (dispatch) => {
    try {
        const { data } = await axiosInstance.get();
        dispatch(ActionCreators.setExpenses(data))
    } catch (error) {
        console.log(error)
    }
}

export const NewExpense = async (dispatch, expense) => {
    try {
        const { data } = await axiosInstance.post('', expense);
        dispatch(ActionCreators.newExpense(data))
    } catch (error) {
        console.log(error)
    }
}

export const EditExpense = async (dispatch, expense) => {
    try {
        await axiosInstance.put('', expense);
        dispatch(ActionCreators.editExpense(expense))
    } catch (error) {
        console.log(error)
    }
}

export const DeleteExpense = async (dispatch, expense) => {
    try {
        await axiosInstance.delete('', {data: {...expense}});
        dispatch(ActionCreators.deleteExpense(expense))
    } catch (error) {
        console.log(error)
    }
}